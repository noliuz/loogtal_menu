import React from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import foodCSV from '../csv/menu.csv'
import categoryCSV from '../csv/category.csv'

export default class TableComponent extends React.Component {
  fullMenuArr
  constructor() {
    super()

    this.state = {
      cateArr:[],
      menuArr:[],
      cateSelected:0
    }

    this.cateChange = this.cateChange.bind(this)
  }

  async getCate() {
    let csv = await fetch(categoryCSV)
    let txt = await csv.text()
    let lines =  txt.split("\n")
    let arr = []
    lines.forEach((item)=> {
      if (item.trim().length !== 0) {
        let tmpArr = item.split(',')
        let json = {id:tmpArr[0],name:tmpArr[1]}
        arr.push(json)
      }
    })
    this.setState({cateArr:arr})
  }

  async getMenu() {
    let csv = await fetch(foodCSV)
    let txt = await csv.text()
    let lines = txt.split("\n")
    let resArr = []
    lines.forEach((item)=> {
      if (item.trim().length !== 0) {
        let itemArr = item.split(",")
        let name = itemArr[1].trim()
        let price = itemArr[2]
        let cateNo = itemArr[0]
        let cateName = ''

        //find cateName
        this.state.cateArr.forEach((item)=> {
          if (item.id === cateNo) {
            cateName = item.name
            return
          }
        })

        resArr.push({
          name:name,cateName:cateName,cateNo:cateNo,price:price
        })
      }
    })
    this.setState({menuArr:resArr,fullMenuArr:resArr})
    //console.log(resArr)
  }

  componentDidMount() {
    this.getCate()
      .then(()=> {
        this.getMenu(()=>{
          console.log(this.state.menuArr)
        })
      })

  }

  async cateChange(e) {
    let val = e.target.value
    this.setState({cateSelected:val})

    if (val == 0) {
      this.setState({menuArr:this.state.fullMenuArr})
    } else {
      let res = this.state.fullMenuArr.filter(item=> item.cateNo===val)
      //console.log(val)
      this.setState({menuArr:res})
    }
  }

  render() {
    return (

      <TableContainer component={Paper}>
        <Table >
          <TableHead>
            <TableRow>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={this.state.cateSelected}
                onChange={this.cateChange}
              >
                <MenuItem value={0} >อาหารทุกหมวด</MenuItem>
                {
                  this.state.cateArr.map(item=>(
                    <MenuItem value={item.id}>{item.name}</MenuItem>

                  ))
                }


              </Select>
            </TableRow>
            <TableRow style={{backgroundColor:'lightgreen'}}>
              <TableCell style={{fontSize:25,fontWeight:'bold'}}>ชื่อ</TableCell>
              <TableCell style={{fontSize:25,fontWeight:'bold'}}>ราคา</TableCell>
              <TableCell style={{fontSize:25,fontWeight:'bold'}}>หมวด</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>

            {
              this.state.menuArr.map((item)=> (
                <TableRow key={item.name}>
                  <TableCell>{item.name}</TableCell>
                  <TableCell>{item.price}</TableCell>
                  <TableCell>{item.cateName}</TableCell>
                </TableRow>
              ))
            }


          </TableBody>
        </Table>
      </TableContainer>

    )
  }
}
