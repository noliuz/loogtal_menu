import React from 'react';
import './App.css';
import Paper from '@material-ui/core/Paper';
import TableComponent from './components/TableComponent'

function App() {
  document.title = 'เมนูร้านลูกตาล'

  return (
    <div className="App">
      <header className="App-header">
        สั่งโทร 044-812-728 , 081-068-5250 
        <TableComponent />

      </header>
    </div>
  );
}

export default App;
